<?php include '_master/header.php'; ?>
<?php include 'lists_db.php'; ?>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">รายการทั้งหมด             
                </h1>
                <h3></h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">รายการทั้งหมด</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <!-- Blog Entries Column -->

            <div class="col-md-8">
                <div class="col-md-12">
                </div>    
                <div class="col-md-12" >
                        <table class="table table-striped" style="width:100%">         
                            <tbody>  
                                <?php  
                                    while ($row = mysqli_fetch_assoc($result)) {  
                                        switch ($row['category_announce']) {
                                                    case '0':
                                                        $category_announce = "ขาย";
                                                        break;
                                                    
                                                    case '1':
                                                        $category_announce = "เช่า";
                                                        break;
                                                };
                                ?>  
                                    <tr> 
                                        <td width="220">
                                            
                                            <img src="<?php echo $row['img_announce_1']; ?>" alt="Smiley face" height="190" width="220">                                    
                                        </td>       
                                        <td>
                                            
                                            <h3><b style="color:#00CC99"><?php echo $row['announce']; ?></b></h3>
                                            <h4 style="color:grey"><?php echo $row['category']; ?> - <?php echo $category_announce; ?> </h4>
                                            <h4 style="color:grey;font-size:12px"><?php echo $row['road']; ?>, <?php echo $row['DISTRICT_NAME']; ?>, <?php echo $row['AMPHUR_NAME']; ?>, <?php echo $row['PROVINCE_NAME']; ?> </h4>
                                            <h4 style="color:grey;font-size:12px"><?php echo $row['living_area']; ?> ตารางเมตร, <?php echo $row['price']; ?> บาท</h4>
                                            <h4 style="color:grey;font-size:15px"><b><?php echo $row['bedroom']; ?></b> <img src="images/bed.png" alt="Smiley face" height="17" width="17"> , <b><?php echo $row['bathroom']; ?></b> <img src="images/bath.png" alt="Smiley face" height="17" width="17">  </h4>
                                        </td>
                                        <td >

                                            <h3 align="right" style="font-size:12px"><b><?php echo number_format($row['price']); ?> บาท</b></h3>
                                            <br><br><br><br><br>
                                            <a style="margin-left:20%" href="detail_announces.php?id_announce=<?php echo $row['id_announce']; ?>" class="btn btn-primary">ข้อมูลเพิ่มเติม</a>                         
                                        </td>  
                                    </tr> 
                                <?php  
                                    };  
                                ?>  
                             </tbody>  
                                   
                                </table>
                                <div class="col-md-offset-3">
                                 <?php  
                                       $sql = "SELECT COUNT(id_announce) FROM announces";  
                                       $result = mysqli_query($conn,$sql);  
                                       $row = mysqli_fetch_array($result); 
                                       $total_records = $row[0];  
                                       $total_pages = ceil($total_records / $limit);  
                                       $pagLink = "<div class='pagination'>";  
                                       for ($i1=1; $i1<=$total_pages; $i1++) {  
                                       $pagLink .= "<li><a href='lists.php?page=".$i1."'>".$i1."</a></li>";  
                                       };  
                                       echo $pagLink . "</div>";  
                                        mysqli_close($conn);
                                ?>
                                </div>
                    <hr>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

            </div>

        </div>
<?php include '_master/footer.php'; ?>