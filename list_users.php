<?php include '_master/_header.php'; ?>
<?php include 'get_users.php'; ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     รายละเอียดข้อมูลสมาชิก
                  </h3>
              </div>

              <div class="title_right">
                
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small>รายชื่อสมาชิก</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th align="center" width="1%">ลำดับ</th>
                                <th>อีเมล์</th>
                                <th>ชื่อ-นามสกุล</th>
                                <th>บริษัท</th>
                                <th>สถานะ</th>
                                <th width="20%">#จัดการ</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                               <?php  $i = 1;
                                 while ($row = mysqli_fetch_assoc($result)) {  
                               ?>
                               <tr>  
                                 <td align="center" ><?php echo $i; $i++; ?></td>  
                                 <td><?php echo $row["email"]; ?></td> 
                                 <td><?php echo $row["firstname"].' '.$row['lastname']; ?></td>  
                                 <td><?php if($row['company_name'] != ''){echo $row["company_name"];}else{echo "-";} ?></td>  
                                 <td><?php echo $row["status"]; ?></td> 

                                 <td><a class="btn btn-xs btn-primary" href="list_users_profile.php?id_member=<?php echo $row['id_member'];?>"><span class="fa fa fa-folder" ></span> ข้อมูล</a>

                                  <?php echo '<a class="btn btn-xs btn-info" data-toggle="modal" data-id="'. $row['id_member'] .'" href="#myModalP'. $row['id_member'] .'"  ><span class="fa fa fa-pencil" ></span> แก้ไข</a>'; ?>

                                  <?php echo '<a class="btn btn-xs btn-danger" data-toggle="modal" data-id="'. $row['id_member'] .'" href="#myModal'. $row['id_member'] .'"  ><span class="fa fa-trash-o" ></span> ลบ</a></td>'; ?>

                                  <!-- Modal Delete-->
                                  <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['id_member']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">แน่ใจหรือไม่</h4>
                                        </div>
                                        <div class="modal-body">
                                          คุณต้องการลบสมาชิกชื่อ <?php echo $row['firstname'].' '.$row['lastname']; ?>
                                        </div>
                                        <div class="modal-footer">
                                          <a class="btn btn-danger" href="del_users_db.php?id_member=<?php echo $row['id_member']?>">ตกลง</a>
                                          <a class="btn btn-default" data-dismiss="modal">ยกเลิก</a>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- Modal Delete-->
                                  </div>
                                  <!-- Modal Delete-->

                                  
                                  <!-- Modal Edit-->
                                  <div class="modal fade" id="myModalP<?php echo $row['id_member']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูลส่วนตัว</h4>
                                        </div>
                                        <div class="modal-body">
                                          <form data-parsley-validate class="form-horizontal form-label-left" action="update_profile.php" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อ <span class="required" style="color:red">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="firstname" value="<?php echo $row['firstname'];?>" name="firstname" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">นามสกุล <span class="required" style="color:red">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="lastname" value="<?php echo $row['lastname'];?>" name="lastname" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">บริษัท 
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="company_name" value="<?php echo $row['company_name'];?>" name="company_name" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ตำแหน่ง 
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="position" value="<?php echo $row['position'];?>" name="position" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">เพศ 
                                                  <span class="required" style="color:red">*</span></label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select class="form-control" name="gender">
                                                    <?php if ($row['gender'] == 'm'): ?>
                                                      <option value="m">Male</option>
                                                      <option value="f">Female</option>
                                                    <?php endif ?>
                                                    <?php if ($row['gender'] == 'f'): ?>
                                                      <option value="f">Female</option> 
                                                      <option value="m">Male</option>                                  
                                                    <?php endif ?>                             
                                                    </select>
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ที่อยู่ <span class="required" style="color:red">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <textarea type="text" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" rows="3"><?php echo $row['address']; ?></textarea>
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เบอร์โทรศัพท์ <span class="required" style="color:red">*</span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="phone_no" value="<?php echo $row['phone_no'];?>" name="phone_no" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เแฟกซ์
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="fax" value="<?php echo $row['fax'];?>" name="fax" required="required" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="form-group">
                                                  <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูป <span class="required"></span>
                                                  </label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="file" id="last-name" name="img_member_n" class="form-control col-md-7 col-xs-12">
                                                  </div>
                                                </div>
                                            </div>
                                            <br>
                                            <!-- Hidden ID -->
                                            <input type="hidden" name="id_member" value="<?php echo $row['id_member']; ?>">
                                            <input type="hidden" name="img_member" value="<?php echo $row['img_member']; ?>">
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          <!-- <button type="submit" class="btn btn-success">Save changes</button> -->
                                          <button type="submit" name="submitp2" class="btn btn-success">Submit</button>
                                        </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Modal Edit--> 	
                                  
                               </tr>
                               <?php  
                                 };  
                               ?>
                                
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>

<?php include '_master/_footer.php' ?>