<?php include '_master/header.php'; ?>
<?php include 'dash_change_pass.php'; ?>
<?php 
    if(!isset($_SESSION['login_user'])){
      header("location: login_users.php");
      } 
?>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">แก้ไขข้อมูลส่วนตัว             
                </h1>
                <h3></h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">แก้ไขข้อมูลส่วนตัว</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-3">

                <!-- Blog Search Well -->
                <div class="well">
                    
                    <div class="input-group">
                        <div class="col-md-8 col-md-offset-2">
                          <img class="img-circle" width="100%" src="<?php echo $row['img_member']; ?>" alt="ไม่มีรูป" title="">
                        </div>
                        <br>
                        <h3><?php echo $row['firstname'].' '.$row['lastname']; ?></h3>
                        <ul class="list-unstyled user_data">
                          <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $row['address']; ?>
                          </li>
                          <?php if ($status == "agent"): ?>
                           <li>
                            <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $row['company_name']; ?>
                          </li> 
                          <?php endif ?>
                          

                          <li class="m-top-xs">
                            <i class="fa fa-phone user-profile-icon"></i> <?php echo $row['phone_no']; ?>                       
                          </li>
                        </ul>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                          <i class="fa fa-edit m-right-xs"></i>แก้ไขข้อมูลส่วนตัว
                        </button>
                        <!-- Modal Edit-->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูลส่วนตัว</h4>
                              </div>
                              <div class="modal-body">
                                <form data-parsley-validate class="form-horizontal form-label-left" action="update_profile.php" method="post" enctype="multipart/form-data">
                                  <div class="row">
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อ <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="firstname" value="<?php echo $row['firstname'];?>" name="firstname" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">นามสกุล <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="lastname" value="<?php echo $row['lastname'];?>" name="lastname" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                   <?php if ($status == "agent"): ?>
                                      <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อบริษัท 
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="company_name" value="<?php echo $row['company_name'];?>" name="company_name" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ตำแหน่ง 
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="position" value="<?php echo $row['position'];?>" name="position" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                   <?php endif ?>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12">เพศ 
                                      <span class="required" style="color:red">*</span></label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="gender">
                                        <?php if ($row['gender'] == 'm'): ?>
                                          <option value="m">Male</option>
                                          <option value="f">Female</option>
                                        <?php endif ?>
                                        <?php if ($row['gender'] == 'f'): ?>
                                          <option value="f">Female</option> 
                                          <option value="m">Male</option>                                  
                                        <?php endif ?>                             
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ที่อยู่ <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea type="text" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" rows="3"><?php echo $row['address']; ?></textarea>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เบอร์โทรศัพท์ <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="phone_no" value="<?php echo $row['phone_no'];?>" name="phone_no" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">แฟกซ์ 
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="fax" value="<?php echo $row['fax'];?>" name="fax" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูป <span class="required"></span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" id="last-name" name="img_member_n" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Hidden ID -->
                                  <input type="hidden" name="page" value="1">
                                  <input type="hidden" name="id_member" value="<?php echo $row['id_member']; ?>">
                                  <input type="hidden" name="img_member" value="<?php echo $row['img_member']; ?>">
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                <!-- <button type="submit" class="btn btn-success">Save changes</button> -->
                                <button type="submit" name="submitp" class="btn btn-success">Submit</button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- Modal Edit-->
                    </div>
                    <!-- /.input-group -->
                </div>

            </div>
            <!-- Blog Entries Column -->
            <div class="col-md-9">
                <div class="col-md-12">
                </div>    
                <div class="col-md-12" >
                	<div class="panel panel-info">
                        <div class="panel-heading">รายละเอียดข้อมูลส่วนตัว</div>
                        <div class="panel-body">
                            <div class="row">
                            
                                <label  class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >อีเมล์ :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['email']; ?> 
                                </label>
                              </div>
                              <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >ชื่อ-นามสกุล :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['firstname'].' '.$row['lastname']; ?> 
                                </label>
                              </div>
                              <?php if ($status == "agent"): ?>
                                <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >บริษัท :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['company_name']; ?> 
                                </label>
                              </div>
                              <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >ตำแหน่ง :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['position']; ?> 
                                </label>
                              </div>
                              <?php endif ?>
                              <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >ที่อยู่ :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['address']; ?> 
                                </label>
                              </div>
                              <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >เบอร์โทรศัพท์ :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['phone_no']; ?> 
                                </label>
                              </div>
                              <br>
                              <div class="row">
                                <label class="control-label col-md-3 col-sm-2 col-xs-12" align="right" >แฟกซ์ :
                                </label>
                                <label style="color:grey" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['fax']; ?> 
                                </label>
                           
                            </div>
                        </div>
                    </div>
                	
                	<hr>
                </div>

            </div>

           
            

        </div>
<?php include '_master/footer.php'; ?>