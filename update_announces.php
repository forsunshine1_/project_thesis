<?php include '_master/_header.php'; ?>
<?php include 'get_announces.php'; ?>
<?php include 'get_p_a_d.php'; ?>
<style type="text/css">
	 #mapnew {
        height: 100%;
      }
</style>
        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>เพิ่มข้อมูลประกาศ </h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small>กรอกรายละเอียด</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<?php if ($result_msg != ''): ?>
                      <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $result_msg;unset($_SESSION['result_msg']); ?>
                      </div>
                    <?php endif ?>
                    <br />
                    <!-- Smart Wizard -->
                   <form class="form-horizontal form-label-left" id="form_vali" method="post" action="add_announces_db.php"  enctype="multipart/form-data" >
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              ขั้นตอนที่ 1<br />
                                              <small>ข้อมูลทั่วไป</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              ขั้นตอนที่ 2<br />
                                              <small>รายละเอียดเพิ่มเติม</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              ขั้นตอนที่ 3<br />
                                              <small>รูปภาพประกอบ</small>
                                          </span>
                          </a>
                        </li>
                      </ul>
                      
                      <div id="step-1">
                      	ข้อมูลเบื้องต้น
                      	<div class="ln_solid"></div>
                        
                        	<div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >ประเภทประกาศ <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
                        		</label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <p>
			                        ขาย:
			                        <input type="radio" class="flat" name="category_announce"  value="0" <?php if ($row4['category_announce'] == 0) {
			                        	echo "checked=";
			                        }  ?>required />&nbsp;&nbsp; เช่า:
			                        <input type="radio" class="flat" name="category_announce" <?php if ($row4['category_announce'] != 0) {
			                        	echo " checked= ";
			                        }  ?> value="1" />
			                      </p>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ประเภท 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <select class="form-control" name="category">
			                        <option value="บ้าน">บ้าน</option>
			                        <option value="คอนโด">คอนโด</option>
		                            <option value="อพาร์ทเม้นท์">อพาร์ทเม้นท์</option>                                       
		                          </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">หัวข้อประกาศ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <input type="text" id="last-name" name="announce" class="form-control col-md-7 col-xs-12" class="required" value="<?php echo $row4['announce']; ?>">
			                          <input type="hidden" id="last-name" name="id_member" value="" class="form-control col-md-7 col-xs-12" >			                      
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">รายละเอียดเกี่ยวกับประกาศ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <textarea class="resizable_textarea form-control" placeholder="" required="required" name="detail_announce" rows="3"><?php echo $row4['detail_announce']; ?></textarea>		                      
		                        </div>
		                    </div>
		                    รายละเอียดที่ตั้ง
                      		<div class="ln_solid"></div>
                      		<div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">จังหวัด 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <select class="form-control" name="PROVINCE_ID">
		                          	   <option value="<?php echo $row4["PROVINCE_ID"]; ?>"><?php echo $row4["PROVINCE_NAME"]; ?></option>
		                               <option value="<?php echo $row1["PROVINCE_ID"]; ?>"><?php echo $row1["PROVINCE_NAME"]; ?></option>                                 
		                          </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">เขต / อำเภอ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <select class="form-control" name="AMPHUR_ID">
		                          		<option value="<?php echo $row4["AMPHUR_ID"]; ?>"><?php echo $row4["AMPHUR_NAME"]; ?></option>
		                                <?php while ($row2 = mysqli_fetch_assoc($amphur)) { ?>
		                                	<option value="<?php echo $row2["AMPHUR_ID"]; ?>"><?php echo $row2["AMPHUR_NAME"]; ?></option>
		                                <?php } ?>                                  
		                          </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">แขวง / ตำบล 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <select class="form-control" name="DISTRICT_ID">
		                          	<option value="<?php echo $row4["DISTRICT_ID"]; ?>"><?php echo $row4["DISTRICT_NAME"]; ?></option>
		                             <?php while ($row3 = mysqli_fetch_assoc($district)) { ?>
		                                <option value="<?php echo $row3["DISTRICT_ID"]; ?>"><?php echo $row3["DISTRICT_NAME"]; ?></option>
		                              <?php } ?>                                      
		                          </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ชื่อโครงการ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <input type="text" id="last-name" name="project_name" value="<?php echo $row4["project_name"]; ?>" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">เลขที่ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <input type="text" id="last-name" name="home_no" value="<?php echo $row4["home_no"]; ?>" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ถนน 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <input type="text" id="last-name" name="road" value="<?php echo $row4["road"]; ?>" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">รหัสไปรษณีย์ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">		                          
			                          <input type="text" id="last-name" name="zipcode" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">แผนที่ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12" style="height:480px"> 		                          
			                         <div id="mapnew" class="form-control col-md-7 col-xs-12"></div>
									   <div id="latlong">
									    <input size="20" type="hidden" id="latbox" name="latitude" >
									    <input size="20" type="hidden" id="lngbox" name="longitude" >
									  </div>  			                      
		                        </div>
		                    </div>
		                   
		                    ข้อมูลด้านราคา
                      		<div class="ln_solid"></div>
                      		<div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ราคา 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
			                        <input type="text" class="form-control" id="inputSuccess5" name="price" >
			                        <span class="form-control-feedback right" aria-hidden="true">บาท</span>
			                    </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tenure 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <select class="form-control" name="tenure">
		                            <option value="ขายขาด">ขายขาด</option>                                       
		                          </select>
		                        </div>
		                    </div>
		                    ขนาดเพิ่มเติม
                      		<div class="ln_solid"></div>
                      		<div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">พื้นที่ใช้สอย 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
			                        <input type="text" class="form-control" id="inputSuccess5" name="living_area" >
			                        <span class="form-control-feedback right" aria-hidden="true">ตร.ม.</span>
			                    </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">ขนาดพื้นที่ 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-3 col-sm-3 col-xs-6">		                          
			                          <input type="text" name="living_wide" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                        <div class="col-md-3 col-sm-3 col-xs-6">		                          
			                          <input type="text" name="living_long" class="form-control col-md-7 col-xs-12">			                      
		                        </div>
		                    </div>
		                    <br>
		                    <div class="form-group">
		                        <label class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนที่ดิน 
		                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
		                        </label>
		                        <div class="col-md-2 col-sm-2 col-xs-4">		                          
			                          <input type="text" name="land_rhai" class="form-control col-md-7 col-xs-12">
			                          <span class="form-control-feedback right" aria-hidden="true">ไร่</span>			                      
		                        </div>
		                        <div class="col-md-2 col-sm-2 col-xs-4">		                          
			                          <input type="text" name="land_ngan" class="form-control col-md-7 col-xs-12">	
			                          <span class="form-control-feedback right" aria-hidden="true">งาน</span>		                      
		                        </div>
		                        <div class="col-md-2 col-sm-2 col-xs-4">		                          
			                          <input type="text" name="land_wa" class="form-control col-md-7 col-xs-12">	
			                          <span class="form-control-feedback right" aria-hidden="true">ตร.วา</span>		                      
		                        </div>
		                    </div>
                        	
                      </div>
                      <div id="step-2" style="height:1300px">
	                        รายละเอียดเพิ่มเติม
	                      	<div class="ln_solid"></div>
	                      	
	                      	<div class="row">
		                   		<div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนห้องนอน 
				                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
				                     </label>
				                     <div class="col-md-6 col-sm-6 col-xs-12">
				                        <select class="form-control" name="bedroom">
				                            <option value="1">1 ห้องนอน</option>
				                            <option value="2">2 ห้องนอน</option>
				                            <option value="3">3 ห้องนอน</option>
				                            <option value="4">4 ห้องนอน</option>
				                            <option value="5">5 ห้องนอน</option>
				                            <option value="6">6 ห้องนอน</option>
				                            <option value="7">7 ห้องนอน</option>
				                            <option value="8">8 ห้องนอน</option>
				                            <option value="9">9 ห้องนอน</option>
				                            <option value="10">10 ห้องนอน</option>
				                            <option value="10+">10+ ห้องนอน</option>	                                   
				                        </select>
				                    </div>
				                </div>
			                </div>
			                <br>
			                <div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนห้องน้ำ 
				                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
				                     </label>
				                     <div class="col-md-6 col-sm-6 col-xs-12">
				                        <select class="form-control" name="bathroom">
				                            <option value="1">1 ห้อง</option>
				                            <option value="2">2 ห้อง</option>
				                            <option value="3">3 ห้อง</option>
				                            <option value="4">4 ห้อง</option>
				                            <option value="5">5 ห้อง</option>
				                            <option value="6">6 ห้อง</option>
				                            <option value="7">7 ห้อง</option>
				                            <option value="8">8 ห้อง</option>
				                            <option value="9">9 ห้อง</option>                                  
				                        </select>
				                    </div>
				                </div> 
				            </div>
				            <br>
				            <div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">จำนวนชั้น 
				                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
				                     </label>
				                     <div class="col-md-6 col-sm-6 col-xs-12">
				                        <select class="form-control" name="floor_area">
				                            <?php for ($i=1; $i < 51 ; $i++) { 
				                            	echo '<option value="'.$i.'">'.$i.'</option>';
				                            } ?>                                 
				                        </select>
				                    </div>
				                </div> 
				            </div>
				            <br> 
				            <div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">อยู่ชั้น 
				                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
				                     </label>
				                     <div class="col-md-6 col-sm-6 col-xs-12">
				                        <select class="form-control" name="floor">
				                            <?php for ($i=1; $i < 51 ; $i++) { 
				                            	echo '<option value="'.$i.'">'.$i.'</option>';
				                            } ?>                                 
				                        </select>
				                    </div>
				                </div> 
				            </div>
				            <br> 
				            <div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12">เฟอร์นิเจอร์ 
				                        <span class="required" style="color:red">*</span>&nbsp;&nbsp;:
				                     </label>
				                     <div class="col-md-6 col-sm-6 col-xs-12">
				                        <select class="form-control" name="furniture">
				                            <option value="1">ไม่มี</option>
				                            <option value="2">มีบางส่วน</option>
				                            <option value="3">ครบชุด</option>                            
				                        </select>
				                    </div>
				                </div>  
				            </div>
				            อุปกรณ์ตกแต่ง
	                      	<div class="ln_solid"></div>
				            <div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12"> 
				                        <span class="required" style="color:red"></span>&nbsp;&nbsp;
				                     </label>
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="tv" value="1" data-parsley-mincheck="2" required class="flat" /> TV
				                        <br><br>

				                        <input type="checkbox" name="fridge" value="1" class="flat" /> ตู้เย็น
				                        <br><br>

				                        <input type="checkbox" name="wardrobe" value="1" class="flat" /> ตู้เสื้อผ้า
				                        <br><br>

				                        <input type="checkbox" name="bathtub" value="1" class="flat" /> อ่างอาบน้ำ
				                     </div>
				                        
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="bed" value="1" class="flat" /> เตียง
				                        <br><br>
				                        <input type="checkbox" name="waterheater" value="1" class="flat" />  เครื่องทำน้ำอุ่น
				                        <br><br>
				                        <input type="checkbox" name="air" value="1" class="flat" />  เครื่องปรับอากาศ
				                     </div>
				                </div>  
				            </div>
				            <br>  	
                        	ลักษณะพิเศษ
	                      	<div class="ln_solid"></div>
	                      	<div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12"> 
				                        <span class="required" style="color:red"></span>&nbsp;&nbsp;
				                     </label>
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="sea_view" value="1" data-parsley-mincheck="2" required class="flat" /> วิวทะเล
				                        <br><br>

				                        <input type="checkbox" name="pool_view" value="1" class="flat" /> วิวสระว่ายน้ำ
				                        <br><br>

				                        <input type="checkbox" name="garden" value="1" class="flat" /> สวน
				                     </div>
				                        
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="city_view" value="1" class="flat" /> วิวเมือง
				                        <br><br>
				                        <input type="checkbox" name="card_access" value="1" class="flat" />  บัตรผ่านเข้า-ออก
				                        <br><br>
				                        <input type="checkbox" name="balcony" value="1" class="flat" />  ระเบียง
				                     </div>
				                </div>  
				            </div>
				            สิ่งอำนวยความสะดวก
	                      	<div class="ln_solid"></div>
	                      	<div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12"> 
				                        <span class="required" style="color:red"></span>&nbsp;&nbsp;
				                     </label>
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="security" value="1" required class="flat" /> รักษาความปลอภัย 24 ชม.
				                        <br><br>
				                        <input type="checkbox" name="cctv" value="1" class="flat" /> กล้องวงจรปิด
				                        <br><br>
				                        <input type="checkbox" name="fitness" value="1" class="flat" /> ฟิตเนส
				                        <br><br>
				                        <input type="checkbox" name="library"  value="1" class="flat" /> ห้องสมุด
				                        <br><br>
				                        <input type="checkbox" name="pool"  value="1" class="flat" /> สระว่ายน้ำ
				                     </div>
				                        
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="carpark"  value="1" class="flat" /> ที่จอดรถ
				                        <br><br>
				                        <input type="checkbox" name="minimart"  value="1" class="flat" />  มินิมาร์ท
				                        <br><br>
				                        <input type="checkbox" name="club"  value="1" class="flat" />  สโมสร
				                        <br><br>
				                        <input type="checkbox" name="laundry"  value="1" class="flat" />  อบ ซัก รีด
				                        
				                     </div>
				                </div>  
				            </div>
				            สถานที่ใกล้เคียง
	                      	<div class="ln_solid"></div>
	                      	<div class="row">
				                <div class="form-group">
				                     <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12"> 
				                        <span class="required" style="color:red"></span>&nbsp;&nbsp;
				                     </label>
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="nearby_bts_mrt" value="1" class="flat" /> ใกล้บีทีเอชและเอ็มอาร์ที
				                        <br><br>
				                        <input type="checkbox" name="nearby_market" value="1" class="flat" /> ใกล้ตลาดและร้านสะดวกซื้อ
				                        <br><br>
				                        <input type="checkbox" name="nearby_busterminal"  value="1" class="flat" /> ใกล้สถานีขนส่ง
				                        <br><br>
				                        <input type="checkbox" name="nearby_downtown"  value="1" class="flat" /> ใกล้ตัวเมือง
				                        <br><br>
				                        <input type="checkbox" name="nearby_school"  value="1" class="flat" /> ใกล้โรงเรียน
				                     </div>
				                        
				                     <div class="col-md-3 col-sm-3 col-xs-6">
				                        <input type="checkbox" name="nearby_hospital"  value="1" class="flat" /> ใกล้โรงพยาบาล
				                        <br><br>
				                        <input type="checkbox" name="nearby_shopping"  value="1" class="flat" />  ใกล้ห้างสรรพสินค้า
				                        <br><br>
				                        <input type="checkbox" name="nearby_business"  value="1" class="flat" />  ใกล้แหล่งธุรกิจ
				                        <br><br>
				                        <input type="checkbox" name="nearby_boulevard"  value="1" class="flat" />  ใกล้ถนนสายหลัก
				                        
				                     </div>
				                </div>  
				            </div>
                      </div>
                      <div id="step-3">
                        รูปภาพ
	                    <div class="ln_solid"></div>
                        <div class="row">
            				<div class="form-group">
                                <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูปภาพที่ 1 : <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="last-name" name="img_announce_1" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>            	
                        </div>
                        <br>
                        <div class="row">
            				<div class="form-group">
                                <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูปภาพที่ 2 : <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="last-name" name="img_announce_2" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>            	
                        </div>
                        <br>
                        <div class="row">
            				<div class="form-group">
                                <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูปภาพที่ 3 : <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="last-name" name="img_announce_3" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>            	
                        </div>
                        <br>
                        <div class="row">
            				<div class="form-group">
                                <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูปภาพที่ 4 : <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="last-name" name="img_announce_4" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>            	
                        </div>
                        <br>
                        <div class="row">
            				<div class="form-group">
                                <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูปภาพที่ 5 : <span class="required"></span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" id="last-name" name="img_announce_5" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>            	
                        </div>
                        <br><br>
                      </div>
                      
                    </div>
                    <!-- End SmartWizard Content -->

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFc3Z7eIZ6qE8ruKdGbPmhPVRR5Vd8VKw&callback=initMap">
    </script>
    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Dropzone.js -->
    <script src="vendors/dropzone/dist/min/dropzone.min.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>
    
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <script>
      var map;
      function initMap() {
           
      var latlng = new google.maps.LatLng(13.7244426, 100.3529157);
      var map = new google.maps.Map(document.getElementById('mapnew'), {
          center: latlng,
          zoom: 11,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          title: 'Set lat/lon values for this property',
          draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
          document.getElementById("latbox").value = this.getPosition().lat();
          document.getElementById("lngbox").value = this.getPosition().lng();
      });
            }

    </script>  
    <!-- jQuery Smart Wizard -->
    <script>
      $(document).ready(function() {
        $('#wizard').smartWizard({
   		
       	onFinish: onFinishCallback,
  		
		});

        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'

        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');

	       function onFinishCallback(){
	        $('form').submit();
	    }

      });
    </script>
    <!-- /jQuery Smart Wizard -->
  </body>
</html>