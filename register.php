<?php include '_master/header.php'; ?>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">สมัครสมาชิก             
                </h1>
                <h3></h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">สมัครสมาชิก</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <div class="col-md-12">
                    <div class="panel panel-defualt">
                        <div class="panel-heading"><b>หมายเหตุ</b> : ถ้าคุณต้องการลงประกาศ กรุณาสมัครสมาชิก <a href="register_agent.php">คลิกที่นี่</a></div>   
                    </div>
                </div>    
                <div class="col-md-12" >
                	<div class="panel panel-info">
                        <div class="panel-heading">กรอกรายละเอียด</div>
                        <div class="panel-body">
                            <div class="row">
                             <?php if ($result_msg != ''): ?>
                              <div class="alert alert-success">
                                <strong>Success!</strong> <?php echo $result_msg;unset($_SESSION['result_msg']); ?>
                              </div>
                            <?php endif ?>
                            <form data-parsley-validate class="form-horizontal form-label-left" action="add_users_db.php" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อ <span class="required" style="color:red">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="first-name" name="firstname" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">นามสกุล <span class="required" style="color:red">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="first-name" name="lastname" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">เพศ 
                                        <span class="required" style="color:red">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <select class="form-control" name="gender">
                                            <option value="m">Male</option>
                                            <option value="f">Female</option>                            
                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">เบอร์โทรศัพท์ <span class="required" style="color:red"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="first-name" name="phone_no" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">อีเมล์ <span class="required" style="color:red">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="email" id="first-name" name="email" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label align="right" class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รหัสผ่าน <span class="required" style="color:red">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="password" id="first-name" name="password" required="required" class="form-control col-md-7 col-xs-12">
                                           <input type="hidden" id="first-name" name="register_page" value="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                          
                                          <button type="submit" class="btn btn-success">ลงทะเบียน</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                	
                	<hr>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

            </div>

        </div>
<?php include '_master/footer.php'; ?>