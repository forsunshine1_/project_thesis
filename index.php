<?php include '_master/header.php'; ?>
<?php include 'get_announces.php'; ?> 


<!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide" style="background-image: url(images/city.png);">
        <!-- Indicators -->
        <div class="container">
            <div class="row" style="margin-top:2%;margin-right:7%">
                <div class="col-md-1 col-md-offset-11" >
                    <a href="" class="btn btn-info">ลงประกาศกับเรา</a>
                </div>
            </div>
            <div class="row" style="margin:5%">
                <form  class="form-horizontal " action="update_profile.php" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                       <input
                            type="text"
                            name="prefix[]"
                            placeholder="Prefix"
                            class="form-control"
                            style="float: left; width: 20%;"
                            title="Prefix"
                            >
                        <select name="category_announce" class="form-control" style="float: left; width: 12%;" >
                            <option value="0">ขาย</option>
                            <option value="1">เช่า</option>  
                         </select>
                         <select name="category" class="form-control" style="float: left; width: 12%;" >
                            <option value="บ้าน">บ้าน</option>
                            <option value="คอนโด">คอนโด</option>
                            <option value="อพาร์ทเม้นท์">อพาร์ทเม้นท์</option>    
                         </select>
                         <select name="price" class="form-control" style="float: left; width: 12%;" >
                            <option value="">บ้าน</option>
                            <option value="คอนโด">คอนโด</option>
                            <option value="อพาร์ทเม้นท์">อพาร์ทเม้นท์</option>    
                         </select>
                        <input
                            type="text"
                            name="prefix[]"
                            placeholder="Prefix"
                            class="form-control"
                            style="float: left; width: 10%;"
                            title="Prefix"
                            >
                        <input
                            type="text"
                            name="number[]"
                            placeholder="Number"
                            class="form-control"
                            style="float: left; width: 10%;"
                            >
                      <div class="input-group-btn">

                        <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i> ค้าหา</button>
                      </div>

                    </div>
                </form>
            </div>
        </div>
    </header>  
    <!-- Page Content -->
    <div class="container" >

        <!-- บ้าน -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    บ้าน ขาย-เช่า
                </h1>
            </div>
            <?php                
                for ($i=0; $i < 4 ; $i++) {  
                    while ($row5 = mysqli_fetch_assoc($result5)) {           
            ?>
            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-home"></i> <?php echo $row5['announce'];?></h4>
                    </div>
                    <div class="panel-body" style="height:220px">
                        <img src="<?php echo $row5['img_announce_1'];?>" width="230" height="110" >
                        <p><?php echo $row5['detail_announce']; ?></p>
                        
                    </div>
                    <div class="panel-footer"> 
                        <a href="detail_announces.php?id_announce=<?php echo $row5['id_announce']; ?>" class="btn btn-primary">ข้อมูลเพิ่มเติม</a>
                    </div>
                </div>             
            </div>
            <?php 
                    } ;
                };  
            ?>
            
        </div>
        <hr>
        <div class="col-md-3 col-md-offset-9">
            <div class="col-md-5 col-md-offset-7">
                <a href="lists.php?category=0" align="center" class="btn btn-default">ข้อมูลทั้งหมด ></a>
            </div>    
        </div>
        
        <!-- /.row -->

        <!-- คอนโด -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    คอนโด ขาย-เช่า
                </h1>
            </div>
            <?php                
                for ($i=0; $i < 4 ; $i++) {  
                    while ($row6 = mysqli_fetch_assoc($result6)) {           
            ?>
            <div class="col-md-3">
                <div class="panel panel-info"  >
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-home"></i> <?php echo $row6['announce'];?></h4>
                    </div>
                    <div class="panel-body" style="height:220px">
                        <img src="<?php echo $row6['img_announce_1'];?>" width="230" height="110" >
                        <p><?php echo $row6['detail_announce']; ?></p>
                        
                    </div>
                    <div class="panel-footer"> 
                        <a href="detail_announces.php?id_announce=<?php echo $row6['id_announce']; ?>" class="btn btn-primary">ข้อมูลเพิ่มเติม</a>
                    </div>                   
                </div>
            </div>
            <?php 
                    } ;
                };  
            ?>
            
        </div>
        <hr>
        <div class="col-md-3 col-md-offset-9">
            <div class="col-md-5 col-md-offset-7">
                <a href="lists.php?category=1" align="center" class="btn btn-default">ข้อมูลทั้งหมด ></a>
            </div>    
        </div>
        <!-- /.row -->

        <!-- อพาร์ทเม้นท์ -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    อพาร์ทเม้นท์
                </h1>
            </div>
            <?php                
                for ($i=0; $i < 4 ; $i++) {  
                    while ($row7 = mysqli_fetch_assoc($result7)) {           
            ?>
            <div class="col-md-3">
                <div class="panel panel-info"  >
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-home"></i> <?php echo $row7['announce'];?></h4>
                    </div>
                    <div class="panel-body" style="height:220px">
                        <img src="<?php echo $row7['img_announce_1'];?>" width="230" height="110" >
                        <p><?php echo $row7['detail_announce']; ?></p>
                        
                    </div>
                    <div class="panel-footer"> 
                        <a href="detail_announces.php?id_announce=<?php echo $row7['id_announce']; ?>" class="btn btn-primary">ข้อมูลเพิ่มเติม</a>
                    </div>                   
                </div>
            </div>
            <?php 
                    } ;
                };  
            ?>
        </div>
        <hr>
        <div class="col-md-3 col-md-offset-9">
            <div class="col-md-5 col-md-offset-7">
                <a href="lists.php?category=2" align="center" class="btn btn-default">ข้อมูลทั้งหมด ></a>
            </div>    
        </div>

        <!-- /.row -->

                

<?php include '_master/footer.php'; ?>