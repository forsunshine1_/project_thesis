<?php 
include 'connect.php';
session_start();
$db = mysqli_select_db ($conn,"project_db");
$id_member = $_POST['id_member'];
$category = $_POST['category']; // ประเภทคอนโด
$category_announce = $_POST['category_announce']; //ประเภท ขายเช่า
$announce = $_POST['announce']; //หัวข้อประกาศ
$detail_announce = $_POST['detail_announce']; //รายละเอียดประกาศ
$PROVINCE_ID = $_POST['PROVINCE_ID']; // รหัสจังหวัด
$AMPHUR_ID = $_POST['AMPHUR_ID']; // รหัสอำเภอ
$DISTRICT_ID = $_POST['DISTRICT_ID']; // รหัสแขวง
$project_name = $_POST['project_name']; // ชื่อโครงการ
$home_no = $_POST['home_no']; // บ้านเลขที่
$road = $_POST['road']; // ถนน
$zipcode = $_POST['zipcode']; // รหัสไปรษณีย์
$latitude = $_POST['latitude']; // lat
$longitude = $_POST['longitude']; // lng
$price = $_POST['price']; // ราคา
$tenure = $_POST['tenure']; // ขายขาด
$living_area = $_POST['living_area']; // พื้นที่ใช้สอย
$living_wide = $_POST['living_wide']; // กว้าง
$living_long = $_POST['living_long']; // ยาว
$land_rhai = $_POST['land_rhai']; // ไร่
$land_ngan = $_POST['land_ngan']; // งาน
$land_wa = $_POST['land_wa']; // วา
$bedroom = $_POST['bedroom']; // ห้องนอน
$floor_area = $_POST['floor_area']; // จำนวนชั้น
$floor = $_POST['floor']; // อยู่ชั้น
$bathroom = $_POST['bathroom']; // ห้องน้ำ
$furniture = $_POST['furniture']; // เฟอร์นิเจอร์

if (isset($_POST['tv'])) {
	$tv = $_POST['tv']; // tv
}else{
	$tv = 0;
}
if (isset($_POST['fridge'])) {
	$fridge = $_POST['fridge']; // ตู้เย็น
}else{
	$fridge = 0;
}
if (isset($_POST['wardrobe'])) {
	$wardrobe = $_POST['wardrobe']; // ตู้เสื้อผ้า
}else{
	$wardrobe = 0;
}
if (isset($_POST['bathtub'])) {
	$bathtub = $_POST['bathtub']; // อ่างอาบน้ำ
}else{
	$bathtub = 0;
}
if (isset($_POST['bed'])) {
	$bed = $_POST['bed']; // เตียง
}else{
	$bed = 0;
}
if (isset($_POST['waterheater'])) {
	$waterheater = $_POST['waterheater']; // เครื่องทำน้ำอุ่น
}else{
	$waterheater = 0;
}
if (isset($_POST['air'])) {
	$air = $_POST['air']; // เครื่องปรับอากาศ
}else{
	$air = 0;
}
if (isset($_POST['sea_view'])) {
	$sea_view = $_POST['sea_view']; // วิวทะเล
}else{
	$sea_view = 0;
}
if (isset($_POST['pool_view'])) {
	$pool_view = $_POST['pool_view']; // วิวสระว่ายน้ำ
}else{
	$pool_view = 0;
}
if (isset($_POST['garden'])) {
	$garden = $_POST['garden']; // สวน
}else{
	$garden = 0;
}
if (isset($_POST['city_view'])) {
	$city_view = $_POST['city_view']; // วิวเมือง
}else{
	$city_view = 0;
}
if (isset($_POST['card_access'])) {
	$card_access = $_POST['card_access']; // บัตรผ่านเข้า-ออก
}else{
	$card_access = 0;
}
if (isset($_POST['balcony'])) {
	$balcony = $_POST['balcony']; // ระเบียง
}else{
	$balcony = 0;
}
if (isset($_POST['security'])) {
	$security = $_POST['security']; // รักษาความปลอภัย 24 ชม.
}else{
	$security = 0;
}
if (isset($_POST['cctv'])) {
	$cctv = $_POST['cctv']; // cctv 
}else{
	$cctv = 0;
}
if (isset($_POST['fitness'])) {
	$fitness = $_POST['fitness']; // fitness
}else{
	$fitness = 0;
}
if (isset($_POST['library'])) {
	$library = $_POST['library']; // ห้องสมุด
}else{
	$library = 0;
}
if (isset($_POST['pool'])) {
	$pool = $_POST['pool']; // สระว่ายน้ำ
}else{
	$pool = 0;
}
if (isset($_POST['carpark'])) {
	$carpark = $_POST['carpark']; // ที่จอดรถ
}else{
	$carpark = 0;
}
if (isset($_POST['minimart'])) {
	$minimart = $_POST['minimart']; // มินิมาร์ท
}else{
	$minimart = 0;
}
if (isset($_POST['club'])) {
	$club = $_POST['club']; // สโมสร
}else{
	$club = 0;
}
if (isset($_POST['laundry'])) {
	$laundry = $_POST['laundry']; // อบ ซัก รีด
}else{
	$laundry = 0;
}
if (isset($_POST['nearby_bts_mrt'])) {
	$nearby_bts_mrt = $_POST['nearby_bts_mrt']; // ใกล้บีทีเอชและเอ็มอาร์ที
}else{
	$nearby_bts_mrt = 0;
}
if (isset($_POST['nearby_market'])) {
	$nearby_market = $_POST['nearby_market']; // ใกล้ตลาดและร้านสะดวกซื้อ
}else{
	$nearby_market = 0;
}
if (isset($_POST['nearby_busterminal'])) {
	$nearby_busterminal = $_POST['nearby_busterminal']; // ใกล้สถานีขนส่ง
}else{
	$nearby_busterminal = 0;
}
if (isset($_POST['nearby_downtown'])) {
	$nearby_downtown = $_POST['nearby_downtown']; // ใกล้ตัวเมือง
}else{
	$nearby_downtown = 0;
}
if (isset($_POST['nearby_school'])) {
	$nearby_school = $_POST['nearby_school']; // ใกล้โรงเรียน
}else{
	$nearby_school = 0;
}
if (isset($_POST['nearby_hospital'])) {
	$nearby_hospital = $_POST['nearby_hospital']; // ใกล้โรงพยาบาล
}else{
	$nearby_hospital = 0;
}
if (isset($_POST['nearby_shopping'])) {
	$nearby_shopping = $_POST['nearby_shopping']; // ใกล้ห้างสรรพสินค้า
}else{
	$nearby_shopping = 0;
}
if (isset($_POST['nearby_business'])) {
	$nearby_business = $_POST['nearby_business']; // ใกล้แหล่งธุรกิจ
}else{
	$nearby_business = 0;
}
if (isset($_POST['nearby_boulevard'])) {
	$nearby_boulevard = $_POST['nearby_boulevard']; // ใกล้แหล่งธุรกิจ
}else{
	$nearby_boulevard = 0;
}
if ($_FILES["img_announce_1"]["name"] != "") {
	$temp1 = explode(".", $_FILES["img_announce_1"]["name"]);
	$newfilename1 = round(microtime(true)) . '1.' . end($temp1);
	move_uploaded_file($_FILES["img_announce_1"]["tmp_name"], "images/img_condo/" . $newfilename1);
	 
	$imgpath1 = "images/img_condo/" . $newfilename1 ;
}else{
	$imgpath1 = "";
}
if ($_FILES["img_announce_2"]["name"] != "") {
	$temp2 = explode(".", $_FILES["img_announce_2"]["name"]);
	$newfilename2 = round(microtime(true)) . '2.' . end($temp2);
	move_uploaded_file($_FILES["img_announce_2"]["tmp_name"], "images/img_condo/" . $newfilename2);
	 
	$imgpath2 = "images/img_condo/" . $newfilename2 ;
}else{
	$imgpath2 = "";
}
if ($_FILES["img_announce_3"]["name"] != "") {
	$temp3 = explode(".", $_FILES["img_announce_3"]["name"]);
	$newfilename3 = round(microtime(true)) . '3.' . end($temp3);
	move_uploaded_file($_FILES["img_announce_3"]["tmp_name"], "images/img_condo/" . $newfilename3);
	 
	$imgpath3 = "images/img_condo/" . $newfilename3 ;
}else{
	$imgpath3 = "";
}
if ($_FILES["img_announce_4"]["name"] != "") {
	$temp4 = explode(".", $_FILES["img_announce_4"]["name"]);
	$newfilename4 = round(microtime(true)) . '4.' . end($temp4);
	move_uploaded_file($_FILES["img_announce_4"]["tmp_name"], "images/img_condo/" . $newfilename4);
	 
	$imgpath4 = "images/img_condo/" . $newfilename4 ;
}else{
	$imgpath4 = "";
}
if ($_FILES["img_announce_5"]["name"] != "") {
	$temp5 = explode(".", $_FILES["img_announce_5"]["name"]);
	$newfilename5 = round(microtime(true)) . '5.' . end($temp5);
	move_uploaded_file($_FILES["img_announce_5"]["tmp_name"], "images/img_condo/" . $newfilename5);

	$imgpath5 = "images/img_condo/" . $newfilename5 ;
}else{
	$imgpath5 = "";
}

$status_announce = 1;

$sql = "INSERT INTO announces (id_announce,id_member,category,category_announce,announce,detail_announce,PROVINCE_ID,AMPHUR_ID,DISTRICT_ID,project_name,home_no,road,zipcode,latitude,longitude,living_area,living_wide,living_long,land_rhai,land_ngan,land_wa,price,tenure,bedroom,bathroom,furniture,floor_area,floor,status_announce,img_announce_1,img_announce_2,img_announce_3,img_announce_4,img_announce_5) VALUES ('','$id_member','$category','$category_announce','$announce','$detail_announce','$PROVINCE_ID','$AMPHUR_ID','$DISTRICT_ID','$project_name','$home_no','$road','$zipcode','$latitude','$longitude','$living_area','$living_wide','$living_long','$land_rhai','$land_ngan','$land_wa','$price','$tenure','$bedroom','$bathroom','$furniture','$floor_area','$floor','$status_announce','$imgpath1','$imgpath2','$imgpath3','$imgpath4','$imgpath5')";

if(mysqli_query($conn, $sql)){
	$last_id = $conn->insert_id;

    $other ="INSERT INTO other_announces(id_other_announce,id_announce,tv,fridge,wardrobe,bed,waterheater,air,bathtub,sea_view,city_view,pool_view,balcony,garden,card_access,security,cctv,fitness,library,pool,carpark,minimart,club,laundry,nearby_bts_mrt,nearby_market,nearby_busterminal,nearby_downtown,nearby_hospital,nearby_shopping,nearby_business,nearby_boulevard,nearby_school) VALUES('','$last_id','$tv','$fridge','$wardrobe','$bed','$waterheater','$air','$bathtub','$sea_view','$city_view','$pool_view','$balcony','$garden','$card_access','$security','$cctv','$fitness','$library','$pool','$carpark','$minimart','$club','$laundry','$nearby_bts_mrt','$nearby_market','$nearby_busterminal','$nearby_downtown','$nearby_hospital','$nearby_shopping','$nearby_business','$nearby_boulevard','$nearby_school')";
    if (mysqli_query($conn, $other)) {
    	$_SESSION['result_msg']="ข้อมูลได้ทำการบันทึกเรียบร้อยแล้ว"; // Initializing Session
    }

} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
} 
mysqli_close($conn);

header('Location: add_announces.php'); // Redirecting To Home Page
 ?>