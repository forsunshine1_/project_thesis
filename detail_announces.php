<?php include '_master/header.php'; ?>
<?php include 'get_announces.php'; ?> 



	<header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('<?php echo $row4['img_announce_1'];?>');"></div>
                <div class="carousel-caption">
                    <h2><?php echo $row4['announce']; ?></h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $row4['img_announce_2'];?>');"></div>
                <div class="carousel-caption">
                    <h2><?php echo $row4['announce']; ?></h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $row4['img_announce_3'];?>');"></div>
                <div class="carousel-caption">
                    <h2><?php echo $row4['announce']; ?></h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $row4['img_announce_4'];?>');"></div>
                <div class="carousel-caption">
                    <h2><?php echo $row4['announce']; ?></h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('<?php echo $row4['img_announce_5'];?>');"></div>
                <div class="carousel-caption">
                    <h2><?php echo $row4['announce']; ?></h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $row4['announce']; ?>             
                </h1>
                <h3><?php echo $row4['detail_announce']; ?> </h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">รายละเอียดข้อมูลประกาศ</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <script>
                  var map;
                  function initMap() {
                       
                  var latlng = new google.maps.LatLng(<?php echo $row4['latitude']; ?>, <?php echo $row4['longitude']; ?>);
                  var map = new google.maps.Map(document.getElementById('mapnew'), {
                      center: latlng,
                      zoom: 11,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  });
                  var marker = new google.maps.Marker({
                      position: latlng,
                      map: map,
                      title: 'Set lat/lon values for this property',
                      draggable: false
                  });
                        }
                </script> 
                <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFc3Z7eIZ6qE8ruKdGbPmhPVRR5Vd8VKw&callback=initMap">
                </script> 
                <!-- First Blog Post -->
                <div id="mapnew" style="width:100%;height:380px;"></div>
                <hr id="hr_n">
                <p align="right"><h3>รายละเอียดโครการ</h3></p>
                <div class="col-md-12" >
                	<div class="col-md-3" >
                		<p>ประเภท</p>
                		<p>ชื่อโครงการ</p>
                		<p>อยู่ชั้นที่</p>
                		<p>มี</p>
                		<p>ลงประกาศเมื่อ</p>
                	</div>
                	<div class="col-md-3" >
                	    <p><?php echo $row4['category']; ?></p>
                        <p><?php echo $row4['project_name']; ?></p>
                        <p><?php echo $row4['floor']; ?></p>
                        <p><?php echo $row4['floor_area']; ?> ชั้น</p>
                        <p><?php echo $row4['date']; ?></p>   
                	</div>
                	<div class="col-md-3" >
                		<p>ราคา</p>
                		<p>ห้องนอน</p>
                		<p>ห้องน้ำ</p>
                		<p>เฟอร์นิเจอร์</p>
                	</div>
                	<div class="col-md-2" >
                	   <p align="right"><?php echo number_format($row4['price']); ?></p>
                       <p align="right"><?php echo $row4['bedroom']; ?></p>
                       <p align="right"><?php echo $row4['bathroom']; ?></p>
                       <?php switch ($row4['furniture']) {
                            case '1':
                               $furniture = "ไม่มี";
                               break;
                            case '2':
                               $furniture = "มีบางส่วน";
                               break;
                            case '3':
                               $furniture = "ครบชุด";
                               break;
                       } ?>
                       <p align="right"><?php echo $furniture; ?></p>
                	</div>
                    <div class="col-md-1" >
                       <p>บาท</p>
                       <p>ห้อง</p>
                       <p>ห้อง</p>
                    </div>
                    <br>
                	
                </div>
                <br>
                <p align="right"><h3>สิ่งอำนวยความสะดวก</h3></p>
                <div class="col-md-12" >
                	<div class="col-md-2 col-md-offset-1" >
                        <p align="right">อุปกรณ์ตกแต่ง</p>
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['tv'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ทีวี</p>  
                        <?php endif ?>
                        <?php if ($row4['fridge'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ตู้เย็น</p> 
                        <?php endif ?>
                        <?php if ($row4['wardrobe'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ตู้เสื้อผ้า</p> 
                        <?php endif ?>
                        <?php if ($row4['air'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> แอร์</p> 
                        <?php endif ?>
                        
                    </div>
                    <div class="col-md-3" >
                    
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['bed'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> เตียง</p>  
                        <?php endif ?>   
                        <?php if ($row4['waterheater'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> เครื่องทำน้ำอุ่น</p>  
                        <?php endif ?>
                        <?php if ($row4['bathtub'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> อ่างอาบน้ำ</p>  
                        <?php endif ?>
                        
                    </div>
                    
                </div>
                <br>
                <hr style="border: solid #ddd; border-width: 1px 0 0; clear: both; margin: 22px 0 21px; height: 0;">
                <div class="col-md-12" >
                    <div class="col-md-2 col-md-offset-1" >
                        <p align="right">ลักษณะพิเศษ</p>
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['sea_view'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> วิวทะเล</p>  
                        <?php endif ?>
                        <?php if ($row4['pool_view'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> วิวสระว่ายน้ำ</p>  
                        <?php endif ?>
                        <?php if ($row4['garden'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> สวน</p>  
                        <?php endif ?>
                        
                    </div>
                    <div class="col-md-3" >
                    
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['city_view'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> วิวเมือง</p>  
                        <?php endif ?>
                        <?php if ($row4['card_access'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> บัตรผ่านเข้า-ออก</p>  
                        <?php endif ?>
                        <?php if ($row4['balcony'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ระเบียง</p>  
                        <?php endif ?>
                    </div>
                    <br>
                </div>

                <hr style="border: solid #ddd; border-width: 1px 0 0; clear: both; margin: 22px 0 21px; height: 0;">
                <div class="col-md-12" >
                    <div class="col-md-2 col-md-offset-1" >
                        <p align="right">สิ่งอำนวยความสะดวก</p>
                    </div>
                    <div class="col-md-4" >
                        <?php if ($row4['security'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> รักษาความปลอภัย 24 ชม</p>
                        <?php endif ?>
                        <?php if ($row4['cctv'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> กล้องวงจรปิด</p>
                        <?php endif ?>
                        <?php if ($row4['fitness'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ฟิตเนส</p>
                        <?php endif ?>
                        <?php if ($row4['library'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ห้องสมุด</p>
                        <?php endif ?>
                        <?php if ($row4['pool'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> สระว่ายน้ำ</p>
                        <?php endif ?>
                        
                    </div>
                    <div class="col-md-2" >
                    
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['carpark']== "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ที่จอดรถ</p>
                        <?php endif ?>
                        <?php if ($row4['minimart'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> มินิมาร์ท</p>
                        <?php endif ?>
                        <?php if ($row4['club'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> สโมสร</p>
                        <?php endif ?>
                        <?php if ($row4['laundry'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> อบ ซัก รีด</p>
                        <?php endif ?>
                        
                    </div>       
                </div>
                <hr style="border: solid #ddd; border-width: 1px 0 0; clear: both; margin: 22px 0 21px; height: 0;">
                <div class="col-md-12" >
                    <div class="col-md-2 col-md-offset-1" >
                        <p align="right">สถานที่ใกล้เคียง</p>
                    </div>
                    <div class="col-md-4" >
                        <?php if ($row4['nearby_bts_mrt'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้บีทีเอชและเอ็มอาร์ที</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_market'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้ตลาด</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_busterminal'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้สถานีขนส่ง</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_downtown'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้ตัวเมือง</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_school']== "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้โรงเรียน</p>
                        <?php endif ?>          
                    </div>
                    <div class="col-md-2" >
                    
                    </div>
                    <div class="col-md-3" >
                        <?php if ($row4['nearby_hospital'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้โรงพยาบาล</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_shopping']== "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้ห้างสรรพสินค้า</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_shopping'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้แหล่งธุรกิจ</p>
                        <?php endif ?>
                        <?php if ($row4['nearby_boulevard'] == "1"): ?>
                          <p><img src="images/check.png" width="13px" height="13px"> ใกล้ถนนสายหลัก</p>
                        <?php endif ?>            
                    </div>       
                </div>
                
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Blog Categories</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

                <!-- Side Widget Well -->
                <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>

        </div>
        <hr style="border: solid #ddd; border-width: 1px 0 0; clear: both; margin: 22px 0 21px; height: 0;">
        <div class="row">
            <div class="container">
                 <h3>ติดต่อเจ้าของประกาศ</h3>
                 <div class="col-md-10 col-md-offset-1">                 
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>ชื่อ-นามสกุล</label>
                                <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>เบอร์โทรศัพท์</label>
                                <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>อีเมล์</label>
                                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>ข้อความ</label>
                                <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                            </div>
                        </div>
                        <div id="success"></div>
                        <!-- For success/fail messages -->
                        <button type="submit" class="btn btn-primary">ส่งข้อความ</button>
                    </form>
                 </div>
            </div>
        </div>
<?php include '_master/footer.php'; ?>