<?php include '_master/_header.php'; ?>
<?php include 'get_profile.php'; ?>
<!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>จัดการข้อมูลส่วนตัว</h3>
              </div>

             
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small>ข้อมูลส่วนตัว</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                      <div class="profile_img">

                        <!-- end of image cropping -->
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="<?php echo $row['img_member']; ?>" alt="ไม่มีรูป" title="Change the avatar">

                          

                        </div>
                        <!-- end of image cropping -->

                      </div>
                      <h3><?php echo $row['firstname'].' '.$row['lastname']; ?></h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $row['address']; ?>
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $row['company_name']; ?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone user-profile-icon"></i> <?php echo $row['phone_no']; ?>                       
                        </li>
                      </ul>
                      <br />
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" <?php if(isset($error)){echo "class=\"\"" ;}else{echo "class=\"active\""; } ?> ><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">ข้อมูลส่วนตัว</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" <?php if(isset($error)){echo "class=\"tab-pane fade\"" ;}else{echo "class=\"tab-pane fade active in\""; } ?>  id="tab_content1" aria-labelledby="home-tab">
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >อีเมล์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['email']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ชื่อ-นามสกุล :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['firstname'].' '.$row['lastname']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >บริษัท :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['company_name']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ตำแหน่ง :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['position']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ที่อยู่ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['address']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >เบอร์โทรศัพท์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['phone_no']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >แฟกซ์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['fax']; ?> 
                              </label>
                            </div>
                            <div class="ln_solid"></div>
                          </div>
                          
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a class="btn btn-primary btn-lg" href="list_users.php"><span class="fa fa-arrow-circle-left"></span> ย้อนกลับ</a>
          <br><br><br>
        </div>

        <!-- /page content -->
<?php include '_master/_footer.php'; ?>