<?php include '_master/_header.php'; ?>
<?php include 'get_profile.php'; ?>
<?php include 'dash_change_pass.php'; ?>
<!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>จัดการข้อมูลส่วนตัว</h3>
              </div>

             
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small>ข้อมูลส่วนตัว</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                      <div class="profile_img">

                        <!-- end of image cropping -->
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="<?php echo $row['img_member']; ?>" alt="ไม่มีรูป" title="Change the avatar">

                          

                        </div>
                        <!-- end of image cropping -->

                      </div>
                      <h3><?php echo $row['firstname'].' '.$row['lastname']; ?></h3>

                      <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> <?php echo $row['address']; ?>
                        </li>

                        <li>
                          <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $row['company_name']; ?>
                        </li>

                        <li class="m-top-xs">
                          <i class="fa fa-phone user-profile-icon"></i> <?php echo $row['phone_no']; ?>                       
                        </li>
                      </ul>
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-edit m-right-xs"></i>แก้ไขข้อมูลส่วนตัว
                      </button>
                      <!-- Modal Edit-->
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูลส่วนตัว</h4>
                            </div>
                            <div class="modal-body">
                              <form data-parsley-validate class="form-horizontal form-label-left" action="update_profile.php" method="post" enctype="multipart/form-data">
                                <div class="row">
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อ <span class="required" style="color:red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="firstname" value="<?php echo $row['firstname'];?>" name="firstname" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">นามสกุล <span class="required" style="color:red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="lastname" value="<?php echo $row['lastname'];?>" name="lastname" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ชื่อบริษัท 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="company_name" value="<?php echo $row['company_name'];?>" name="company_name" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ตำแหน่ง 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="position" value="<?php echo $row['position'];?>" name="position" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">เพศ 
                                    <span class="required" style="color:red">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select class="form-control" name="gender">
                                      <?php if ($row['gender'] == 'm'): ?>
                                        <option value="m">Male</option>
                                        <option value="f">Female</option>
                                      <?php endif ?>
                                      <?php if ($row['gender'] == 'f'): ?>
                                        <option value="f">Female</option> 
                                        <option value="m">Male</option>                                  
                                      <?php endif ?>                             
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ที่อยู่ <span class="required" style="color:red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <textarea type="text" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" rows="3"><?php echo $row['address']; ?></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">เบอร์โทรศัพท์ <span class="required" style="color:red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="phone_no" value="<?php echo $row['phone_no'];?>" name="phone_no" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">แฟกซ์ 
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="fax" value="<?php echo $row['fax'];?>" name="fax" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รูป <span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="file" id="last-name" name="img_member_n" class="form-control col-md-7 col-xs-12">
                                    </div>
                                  </div>
                                </div>
                                <!-- Hidden ID -->
                                <input type="hidden" name="id_member" value="<?php echo $row['id_member']; ?>">
                                <input type="hidden" name="img_member" value="<?php echo $row['img_member']; ?>">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                              <!-- <button type="submit" class="btn btn-success">Save changes</button> -->
                              <button type="submit" name="submitp" class="btn btn-success">Submit</button>
                            </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <!-- Modal Edit-->
                      <br />
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" <?php if(isset($error)){echo "class=\"\"" ;}else{echo "class=\"active\""; } ?> ><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">ข้อมูลส่วนตัว</a>
                          </li>
                          <li role="presentation" <?php if(isset($error)){echo "class=\"active\"" ;}else{echo "class=\"\""; } ?>><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">เปลี่ยนรหัสผ่าน</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" <?php if(isset($error)){echo "class=\"tab-pane fade\"" ;}else{echo "class=\"tab-pane fade active in\""; } ?>  id="tab_content1" aria-labelledby="home-tab">
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >อีเมล์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['email']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ชื่อ-นามสกุล :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['firstname'].' '.$row['lastname']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >บริษัท :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['company_name']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ตำแหน่ง :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['position']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >ที่อยู่ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['address']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >เบอร์โทรศัพท์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['phone_no']; ?> 
                              </label>
                            </div>
                            <br>
                            <div class="row">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12" align="right" >แฟกซ์ :
                              </label>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $row['fax']; ?> 
                              </label>
                            </div>
                            <div class="ln_solid"></div>
                          </div>
                          <div role="tabpanel" <?php if(isset($error)){echo "class=\"tab-pane fade active in\"" ;}else{echo "class=\"tab-pane fade\""; } ?> id="tab_content2" aria-labelledby="profile-tab">
                            
                            <form data-parsley-validate class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                <?php if (isset($error)) {
                                  if ($error == "เปลี่ยนรหัสผ่านเรียบร้อย") {
                                    echo "<div class=\"alert alert-success\">
                                          <strong>Success!</strong> เปลี่ยนรหัสผ่านเรียบร้อย
                                        </div>";
                                  }elseif ($error == "รหัสผ่านใหม่ไม่ตรงกัน") {
                                    echo "<div class=\"alert alert-danger\">
                                          <strong>Danger!</strong> รหัสผ่านใหม่ไม่ตรงกัน
                                        </div>";
                                  }elseif($error == "รหัสผ่านเดิมผิด"){
                                    echo "<div class=\"alert alert-danger\">
                                          <strong>Danger!</strong> รหัสผ่านเดิมผิดพลาด
                                        </div>";
                                  }
                                } ?>                    
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสผ่านเดิม <span class="required" style="color:red">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="pass_o" name="pass_o" required="required" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสผ่านใหม่ <span class="required" style="color:red">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="pass_n" name="pass_n" required="required" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>
                                <br>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ยืนยันรหัสผ่านใหม่ <span class="required" style="color:red">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="pass_n2" name="pass_n2" required="required" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="reset" class="btn btn-primary">Reset</button>
                                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                  </div>
                                </div>
                            </form>

                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br><br><br>
        </div>

        <!-- /page content -->
<?php include '_master/_footer.php'; ?>