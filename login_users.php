<?php include '_master/header.php'; ?>
<?php include 'auth.php'; ?>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">เข้าสู่ระบบ             
                </h1>
                <h3></h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">เข้าสู่ระบบ</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <div class="col-md-12">
                </div>    
                <div class="col-md-12" >
                	<div class="panel panel-info">
                        <div class="panel-heading">เข้าสู่ระบบ</div>
                        <div class="panel-body">
                            <div class="row">
                             <?php if ($error != ""): ?>
                              <div class="alert alert-danger">
                                <strong>Alert !</strong> อีเมล์หรือรหัสผ่านผิดพลาด 
                              </div>
                            <?php endif ?>
                            <form data-parsley-validate class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                    <br>
                                    <div class="form-group">
                                        <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">อีเมล์ <span class="required" style="color:red"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="email" id="first-name" name="email" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">รหัสผ่าน <span class="required" style="color:red"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="password" id="first-name" name="password" required="required" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                                          
                                          <button type="submit" name="submit" class="btn btn-success">เข้าสู่ระบบ</button>
                                        </div>
                                    </div>
                                </form>
                                <hr class="col-md-8 col-md-offset-2">
                                <br>
                                <div class="col-md-6 col-md-offset-3">
                                    <p>ลืมรหัสผ่านคลิกที่นี่ <a href="forgot_pass.php">ที่นี่</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                	
                	<hr>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

            </div>

        </div>
<?php include '_master/footer.php'; ?>