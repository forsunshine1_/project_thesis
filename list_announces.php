<?php include '_master/_header.php'; ?>
<?php include 'get_announces.php' ?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     รายละเอียดข้อมูลประกาศ 
                    <?php 
                      switch ($_GET['category']) {
                        case '0':
                          $title = "ขาย-เช่า บ้าน";
                          break;
                        case '1':
                          $title = "ขาย-เช่า คอนโด";
                          break;
                        case '2':
                          $title = "เช่า อพาร์ทเม้นท์";
                          break;  
                      }
                      echo $title;
                     ?>
                  </h3>
              </div>

              <div class="title_right">
                
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small>รายการ</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">ทั้งหมด</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">ขาย</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">เช่า</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                           <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th width="">ลำดับ</th>
                                <th width="">รหัส</th>
                                <th class="a-center ">ยูนิต</th>
                                <th>ราคา</th>
                                <th width="">#จัดการ</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                               <?php  $i = 1;
                                 while ($row = mysqli_fetch_assoc($result)) {  
                               ?>
                               <tr>  
                                 <td align="center" ><?php echo $i; $i++; ?></td>  
                                 <td align="center" ><?php echo $row["id_announce"]; ?></td> 
                                 <td><?php if($row["category_announce"] == 0){echo "ขาย";}else{echo "เช่า";}; ?> - 
                                 <?php echo $row["announce"].' ('.$row["living_area"].' ตารางเมตร)';?>
                                 </td> 
                                 <td><?php echo $row["price"]; ?> บาท</td>  
                                 <td><a class="btn btn-xs btn-primary" href="detail_announces.php?id_announce=<?php echo $row['id_announce'];?>"><span class="fa fa fa-folder" ></span> ข้อมูล</a>

                                  <?php echo '<a class="btn btn-xs btn-info" href="update_announces.php?id_announce='. $row['id_announce'] .'"  ><span class="fa fa fa-pencil" ></span> แก้ไข</a>'; ?>

                                  <?php echo '<a class="btn btn-xs btn-danger" data-toggle="modal" data-id="'. $row['id_announce'] .'" href="#myModal'. $row['id_announce'] .'"  ><span class="fa fa-trash-o" ></span> ลบ</a></td>'; ?>
                                  

                                  <!-- Modal Delete-->
                                  <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row['id_announce']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">แน่ใจหรือไม่</h4>
                                        </div>
                                        <div class="modal-body">
                                          คุณต้องการลบประกาศนี้หรือไม่
                                        </div>
                                        <div class="modal-footer">
                                          <a class="btn btn-danger" href="del_announces_db.php?id_announce=<?php echo $row['id_announce']?>">ตกลง</a>
                                          <a class="btn btn-default" data-dismiss="modal">ยกเลิก</a>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- Modal Delete-->
                                  </div>
                                  <!-- Modal Delete-->
                                  
                               </tr>
                               <?php  
                                 };  
                               ?>
                                
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                              <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive" >
                          <table id="datatable-keytable2" style="width:100%" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th width="">ลำดับ</th>
                                <th width="">รหัส</th>
                                <th class="a-center ">ยูนิต</th>
                                <th>ราคา</th>
                                <th width="">#จัดการ</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                               <?php  $i = 1;
                                 while ($row2 = mysqli_fetch_assoc($result2)) {  
                               ?>
                               <tr>  
                                 <td align="center" ><?php echo $i; $i++; ?></td>  
                                 <td align="center" ><?php echo $row2["id_announce"]; ?></td> 
                                 <td><?php if($row2["category_announce"] == 0){echo "ขาย";}else{echo "เช่า";}; ?> - 
                                 <?php echo $row2["announce"].' ('.$row2["living_area"].' ตารางเมตร)';?>
                                 </td> 
                                 <td><?php echo $row2["price"]; ?> บาท</td>  
                                 <td><a class="btn btn-xs btn-primary" href="detail_announces.php?id_announce=<?php echo $row['id_announce'];?>"><span class="fa fa fa-folder" ></span> ข้อมูล</a>

                                  <?php echo '<a class="btn btn-xs btn-info" data-toggle="modal" data-id="'. $row2['id_member'] .'" href="#myModalP'. $row2['id_member'] .'"  ><span class="fa fa fa-pencil" ></span> แก้ไข</a>'; ?>

                                  <?php echo '<a class="btn btn-xs btn-danger" data-toggle="modal" data-id="'. $row2['id_announce'] .'" href="#myModal'. $row2['id_announce'] .'"  ><span class="fa fa-trash-o" ></span> ลบ</a></td>'; ?>
                                  

                                  <!-- Modal Delete-->
                                  <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row2['id_announce']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">แน่ใจหรือไม่</h4>
                                        </div>
                                        <div class="modal-body">
                                          คุณต้องการลบประกาศนี้หรือไม่
                                        </div>
                                        <div class="modal-footer">
                                          <a class="btn btn-danger" href="del_announces_db.php?id_announce=<?php echo $row2['id_announce']?>">ตกลง</a>
                                          <a class="btn btn-default" data-dismiss="modal">ยกเลิก</a>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- Modal Delete-->
                                  </div>
                                  <!-- Modal Delete-->
                                  
                               </tr>
                               <?php  
                                 };  
                               ?>
                                
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                              <div class="row">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <table id="datatable-keytable3" style="width:100%" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th width="">ลำดับ</th>
                                <th width="">รหัส</th>
                                <th class="a-center ">ยูนิต</th>
                                <th>ราคา</th>
                                <th width="">#จัดการ</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                               <?php  $i = 1;
                                 while ($row3 = mysqli_fetch_assoc($result3)) {  
                               ?>
                               <tr>  
                                 <td align="center" ><?php echo $i; $i++; ?></td>  
                                 <td align="center" ><?php echo $row3["id_announce"]; ?></td> 
                                 <td><?php if($row3["category_announce"] == 0){echo "ขาย";}else{echo "เช่า";}; ?> - 
                                 <?php echo $row3["announce"].' ('.$row3["living_area"].' ตารางเมตร)';?>
                                 </td> 
                                 <td><?php echo $row3["price"]; ?> บาท</td>  
                                 <td><a class="btn btn-xs btn-primary" href="detail_announces.php?id_announce=<?php echo $row['id_announce'];?>"><span class="fa fa fa-folder" ></span> ข้อมูล</a>

                                  <?php echo '<a class="btn btn-xs btn-info" data-toggle="modal" data-id="'. $row3['id_member'] .'" href="#myModalP'. $row3['id_member'] .'"  ><span class="fa fa fa-pencil" ></span> แก้ไข</a>'; ?>

                                  <?php echo '<a class="btn btn-xs btn-danger" data-toggle="modal" data-id="'. $row3['id_announce'] .'" href="#myModal'. $row3['id_announce'] .'"  ><span class="fa fa-trash-o" ></span> ลบ</a></td>'; ?>
                                  

                                  <!-- Modal Delete-->
                                  <div class="modal fade bs-example-modal-sm" id="myModal<?php echo $row3['id_announce']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog modal-sm" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">แน่ใจหรือไม่</h4>
                                        </div>
                                        <div class="modal-body">
                                          คุณต้องการลบประกาศนี้หรือไม่
                                        </div>
                                        <div class="modal-footer">
                                          <a class="btn btn-danger" href="del_announces_db.php?id_announce=<?php echo $row3['id_announce']?>">ตกลง</a>
                                          <a class="btn btn-default" data-dismiss="modal">ยกเลิก</a>
                                        </div>
                                      </div>
                                    </div>
                                    <!-- Modal Delete-->
                                  </div>
                                  <!-- Modal Delete-->
                                  
                               </tr>
                               <?php  
                                 };  
                               ?>
                                
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                        </div>
                      </div>
                    </div>

                   
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
<?php include '_master/_footer.php' ?>