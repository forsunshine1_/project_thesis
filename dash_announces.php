<?php include '_master/_header.php'; ?>
		<div class="right_col" role="main">

          <br />
          <div class="" style="margin-top:8%">
          	จัดการประกาศ
            <div class="ln_solid"></div>
			<div class="row top_tiles">
              <a href="list_announces.php?category=0">
	              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" >
	                <div class="tile-stats" style="background-color:#FFE4E1">
	                  <div class="icon"><i class="fa fa-home"></i>
	                  </div>
	                  <div class="count">บ้าน</div>

	                  <h3 style="color:#696969">จัดการประกาศ</h3>
	                  <p>ขาย-เช่าบ้าน</p>
	                </div>
	              </div>
              </a>    
              <a href="list_announces.php?category=1">
	              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" >
	                <div class="tile-stats" style="background-color:#FFE4E1">
	                  <div class="icon"><i class="fa fa-building"></i>
	                  </div>
	                  <div class="count">คอนโด</div>

	                  <h3 style="color:#696969">จัดการประกาศ</h3>
	                  <p>ขาย-เช่าคอนโด</p>
	                </div>
	              </div>
              </a>    
              <a href="list_announces.php?category=2">
	              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" >
	                <div class="tile-stats" style="background-color:#FFE4E1">
	                  <div class="icon"><i class="fa fa-university"></i>
	                  </div>
	                  <div class="count">อพาร์ทเม้นท์</div>

	                  <h3 style="color:#696969">จัดการประกาศ</h3>
	                  <p>เช่าอพาร์ทเม้นท์</p>
	                </div>
	              </div>
              </a>    
              
            </div>
		</div>
		<div class="" style="margin-top:4%">
          	เพิ่มข้อมูลประกาศ
            <div class="ln_solid"></div>
			<div class="row top_tiles">
              <a href="add_announces.php">
	              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" >
	                <div class="tile-stats" style="background-color:#FFE4E1">
	                  <div class="icon"><i class="fa fa-bullhorn"></i>
	                  </div>
	                  <div class="count">เพิ่ม</div>

	                  <h3 style="color:#696969">เพิ่มข้อมูล</h3>
	                  <p>เพิ่มประการกาศขาย-เช่า</p>
	                </div>
	              </div>
              </a>                        
            </div>
		</div>
	</div>


<?php include '_master/_footer.php'; ?>