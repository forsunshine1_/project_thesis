<?php include 'session2.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">
               <i class="fa fa-home"></i>Project
                    
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="lists.php">ขาย</a>
                    </li>
                    <li>
                        <a href="#">เช่า</a>
                    </li>
                    <li>
                        <a href="lists.php?category=0">บ้าน</a>
                    </li>
                    <li>
                        <a href="lists.php?category=1">คอนโด</a>
                    </li>
                    <li>
                        <a href="lists.php?category=2">อพาร์ทเม้นท์</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">                
                    <li>

                       
                        <?php if (isset($login_session)) {
                            echo "<a href='register.php'><i class='fa fa-bullhorn'></i> ลงประกาศ</a>";
                        }else{
                            
                            echo "<a href='register.php'>สมัครสมาชิก</a>";
                        } ?>
                    </li>
                    
                        <?php if (isset($login_session)) {
                            echo "<li class='dropdown '>";
                            echo " <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".$login_session.' '.$lastname."<span class='caret'></span></a>";
                            echo "<ul class='dropdown-menu'>";
                            echo "<li><a href='edit_profile.php''><i class='fa fa-user'></i> ข้อมูลส่วนตัว</a></li>";
                            echo "<li><a href='edit_pass.php'><i class='fa fa-lock'></i> เปลี่ยนรหัสผ่าน</a></li>";
                            echo "<li><a href='logout.php?out=1'><i class='fa fa-sign-out'></i> ออกจากระบบ</a></li>";
                            echo "</u>";
                        }else{
                            echo "<li>";
                            echo "<a href='login_users.php'>เข้าสู่ระบบ</a>";
                        } ?>
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    