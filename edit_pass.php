<?php include '_master/header.php'; ?>
<?php include 'dash_change_pass.php'; ?>
<?php 
    if(!isset($_SESSION['login_user'])){
      header("location: login_users.php");
      } 
?>
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">แก้ไขรหัสผ่าน             
                </h1>
                <h3></h3>
                <ol class="breadcrumb">
                    <li><a href="index.php">หน้าหลัก</a>
                    </li>
                    <li class="active">แก้ไขรหัสผ่าน</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <div class="col-md-12">
                </div>    
                <div class="col-md-12" >
                	<div class="panel panel-info">
                        <div class="panel-heading">กรอกรายละเอียด</div>
                        <div class="panel-body">
                            <div class="row">
                            <form data-parsley-validate class="form-horizontal form-label-left" action="" method="post" enctype="multipart/form-data">
                                    <div class="col-md-10 col-md-offset-1">
                                    <?php if (isset($error)) {
                                      if ($error == "เปลี่ยนรหัสผ่านเรียบร้อย") {
                                        echo "<div class=\"alert alert-success\">
                                              <strong>Success!</strong> เปลี่ยนรหัสผ่านเรียบร้อย
                                            </div>";
                                      }elseif ($error == "รหัสผ่านใหม่ไม่ตรงกัน") {
                                        echo "<div class=\"alert alert-danger\">
                                              <strong>Alert!</strong> รหัสผ่านใหม่ไม่ตรงกัน
                                            </div>";
                                      }elseif($error == "รหัสผ่านเดิมผิด"){
                                        echo "<div class=\"alert alert-danger\">
                                              <strong>Alert!</strong> รหัสผ่านเดิมผิดพลาด
                                            </div>";
                                      }
                                    } ?> 
                                    </div> 
                                    <br>                  
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสผ่านเดิม <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="pass_o" name="pass_o" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">รหัสผ่านใหม่ <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="pass_n" name="pass_n" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ยืนยันรหัสผ่านใหม่ <span class="required" style="color:red">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="password" id="pass_n2" name="pass_n2" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        
                                        <button type="submit" name="submit" class="btn btn-success">ตกลง</button>
                                      </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                	
                	<hr>
                </div>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

            </div>

        </div>
<?php include '_master/footer.php'; ?>